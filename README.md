# Minecraft Server with a simple plugin

This minecraft server has a simple plugin that adds hearts whenever a player kills something, and removes hearts from players when they re-spawn after dying.

Create a minecraft_data folder before starting the container. Makefile contains the steps for building.