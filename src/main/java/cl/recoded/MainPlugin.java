package cl.recoded;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;


public class MainPlugin extends JavaPlugin {
    @Override
    public void onDisable() {
        // Don't log disabling, Spigot does that for you automatically!
    }

    @Override
    public void onEnable() {
        // Don't log enabling, Spigot does that for you automatically!
        // Register the listener
        getServer().getPluginManager().registerEvents(new PlayerDeathListener(this), this);
    }
}