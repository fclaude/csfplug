package cl.recoded;

import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class PlayerDeathListener implements Listener {
    private final double MAX_HEALTH = 600;
    private final double MIN_HEALTH = 2;

    JavaPlugin _plugin;

    public PlayerDeathListener(JavaPlugin plugin) {
        _plugin = plugin;
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onEntityDeathEvent(EntityDeathEvent e) {
        // Log death
        _plugin.getLogger().info(e.getEntity().getName() + " died (" + e.getEntity().getLastDamageCause().getCause() + ") - killer: " + e.getEntity().getKiller());
        Entity entity = e.getEntity();
        if (entity instanceof Player) {
            Player player = (Player) entity;
            setMaxHealth(-2, player);
        } else {
            _plugin.getLogger().info("Entity is not a player");
        }
        Player killer = e.getEntity().getKiller();
        if (killer != null) {
            setMaxHealth(2, killer);
        }
    }

    public void setMaxHealth(double amount, Player p) {
        AttributeInstance attribute = p.getAttribute(Attribute.GENERIC_MAX_HEALTH);
        double value = (double) attribute.getValue();
        value += amount;
        if (value < MIN_HEALTH) {
            value = MIN_HEALTH;
        }
        if (value > MAX_HEALTH) {
            value = MAX_HEALTH;
        }
        attribute.setBaseValue(value);
        if (value > 0) {
            p.setHealth(p.getHealth() + amount);
        }
    }
}
