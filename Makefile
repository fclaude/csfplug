container:
	podman build -t minecraft-server ./plugin-container

push: container
	podman tag minecraft-server fclaude/family-minecraft:latest
	podman push fclaude/family-minecraft:latest

run: container
	podman run -it -p 25565:25565 -v ./minecraft_data:/minecraft_data --name minecraft-server minecraft	
